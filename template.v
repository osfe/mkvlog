/* ****************************************************************************
 * \file    %FILE%
 * \brief   Please, enter a short description of the module here (1 line)
 *
 * \details If you file talkative, please enter all details you think usefull
 *          here; the more details, the better. Do not comment the code,
 *          explain what the code does, its purpose, its goal.
 *
 * \author  %AUTHOR%, %AUTHOR%@lal.in2p3.fr
 * \date    %DATE%
 * \bug
 * \warning 
 * \todo
 * ***************************************************************************/

`timescale 1 ns/10 ps


module %NAME%
    // parameters
    #(
        parameter name = value
     )

    // ports
    (
        direction wire name,
        direction wire name
    );

    // signals declaration
    reg name;

    // parameters declaration
    localparam name = value;


    // register 
    always @(posedge clk, reset)
        if (reset)
            r_reg <= 0;
        else 
            r_reg <= r_next;

    // next state logic 
    always @*
    begin 
        // please enter the next state logic here 
    end 
endmodule
